ops-flux-local
=====

```bash
// docker registry domain: registry.local-k8s.com
openssl req  -newkey rsa:4096 -nodes -sha256 -keyout certs/registry.local-k8s.com.key -x509 -days 365 -out certs/registry.local-k8s.com.crt
kubectl create secret generic registry-local-k8s-com-tls --from-file=tls.key=certs/registry.local-k8s.com.key --from-file=tls.crt=certs/registry.local-k8s.com.crt --namespace ci --dry-run=client -o yaml > releases/registry-local-k8s-com-tls.yaml
```